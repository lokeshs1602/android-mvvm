# Change log

### All the changes to the project will be recorded in this file with respective version number

## v1.2

## Added

1. Libraries 
	* mockwebserver
	* UIAutomator UI Testing

1. Features Added
	* Setup mockwebserver with dagger for Tests to respond from local file.
	* Updated login and dashboard tests with mock response and throttled the response for timeout.

## v1.1

## Added

1. Libraries 
	* WorkManager
	* Jacoco
	* Sonarqube

1. Features Added
	* Add new user to local DB while OFFLINE.
	* Add new user to remote DB while ONLINE.
	* Sync local DB data using workmanager When connected to internet in a interval of 15 mins and removing it from local db.
	
## v1.0

## Added

1. Libraries 
	* Android X
	* Lifecycle extension(viewmodel & livedata)
	* Dagger 2
	* Retrofit
	* Moshi
	* Testing framework(JUnit,Robolectic,Espresso)
	* Room
	* Realm
	* Coroutine
	* Glide
	* Product flavours

1. Features Added
	* Splash with login check.
	* Login.
	* Sign up.
	* OTP verifcation.
	* Dashboard to load user data.
	* Profile update.
	* Change password.
	* Logout.