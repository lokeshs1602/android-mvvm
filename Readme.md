# MVVM Boiler plate with ROOM/REALM

### This project is indented to describe the implementation and use of MVVM with Room and Realm

## Libraries used for Boiler plate

* Android X
* Lifecycle extension(viewmodel & livedata)
* Dagger 2
* Retrofit
* Moshi
* Testing framework(JUnit,Robolectic,Espresso,UIAutomator)
* Room
* Realm
* Coroutine
* Glide
* Product flavours
* jacoco
* sonarqube

## Feature covered

1. Splash with login check.
1. Login.
1. Sign up.
1. OTP verifcation.
1. Dashboard to load user data.
1. Profile update.
1. Change password.
1. Logout.
1. Add New User.