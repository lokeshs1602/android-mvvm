package com.wk.mvvmboilerplate

import android.content.Context
import android.util.Log
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import okhttp3.mockwebserver.SocketPolicy
import java.util.concurrent.TimeUnit


internal class MockServerDispatcher() {

    /**
     * Return ok response from mock server
     */
    internal inner class RequestDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            Log.i("dispatch",request.path)
            if (request.path?.contains("auth/users/login")!!) {
                return MockResponse().setResponseCode(200).setBody(TestUtils.getContentFromFile("json/login.json"))
            } else if (request.path?.contains("users/5d6fc147db6134570446a607")!!) {
                return MockResponse().setResponseCode(200).setBody(TestUtils.getContentFromFile("json/user_details.json"))
            } else if (request.path?.contains("/users?")!!)
                return MockResponse().setResponseCode(200).setBody(TestUtils.getContentFromFile("json/user_list.json"))

            return MockResponse().setResponseCode(404)
        }
    }

    /**
     * Return error response from mock server
     */
    internal inner class ErrorDispatcher : Dispatcher() {

        override fun dispatch(request: RecordedRequest): MockResponse {
            Log.i("dispatch",request.body.toString())
            return if (request.path?.contains("auth/users/login")!!) {
                MockResponse().setResponseCode(404).setBody(TestUtils.getContentFromFile("json/login_error.json"))
            } else MockResponse().setResponseCode(400)

        }
    }

    /**
     * Return error response from mock server
     */
    internal inner class ThrottleDispatcher : Dispatcher() {

        override fun dispatch(request: RecordedRequest): MockResponse {
            Log.i("dispatch",request.body.toString() + request.requestLine.toString())
            var response = MockResponse()
            response.socketPolicy = SocketPolicy.NO_RESPONSE
            return if (request.path?.contains("auth/users/login")!!) {
                response.setResponseCode(504).throttleBody(1,10,TimeUnit.MILLISECONDS)//.setBody(TestUtils.getContentFromFile("json/login.json"))
            } else MockResponse().setResponseCode(400)

        }
    }
}