package com.wk.mvvmboilerplate

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.wk.mvvmboilerplate.utils.AppUtils

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class Test0AppUtilsInstrumentTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = ApplicationProvider.getApplicationContext<App>()
        assertEquals("com.wk.mvvmboilerplate", appContext.packageName)
    }
    @Test
    fun test_EmailValidation_case1() {
        val case1 = AppUtils.isEmailValid("imran")
        assertEquals(false, case1)
    }
    @Test
    fun test_EmailValidation_case2() {
        val case2 = AppUtils.isEmailValid("imrank@wekancode.com")
        assertEquals(true, case2)
    }
}
