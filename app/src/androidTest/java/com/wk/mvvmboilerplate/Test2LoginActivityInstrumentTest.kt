package com.wk.mvvmboilerplate

import android.view.View
import androidx.lifecycle.Observer
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.idling.CountingIdlingResource
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.runner.RunWith
import android.content.Intent
import androidx.test.espresso.intent.rule.IntentsTestRule
import dagger.android.AndroidInjection
import org.junit.*
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
@LargeTest
class Test2LoginActivityInstrumentTest {
    @get:Rule
    val activityLoginRule = IntentsTestRule(LoginActivity::class.java)

    lateinit var username : ViewInteraction
    lateinit var password : ViewInteraction
    lateinit var login : ViewInteraction
    lateinit var name : ViewInteraction

    private var webServer: MockWebServer? = null


    @Before
    fun setUp(){
        webServer = MockWebServer()
        webServer?.start(8080)

        username = onView(withId(R.id.username)).check(ViewAssertions.matches(isDisplayed()))
        password = onView(withId(R.id.password)).check(ViewAssertions.matches(isDisplayed()))
        login = onView(withId(R.id.login)).check(ViewAssertions.matches(isDisplayed()))

        IdlingRegistry.getInstance().register(activityLoginRule.activity.loginViewModel.idleResource)

    }


    @Test
    fun loginFail_Wrong_User_Cred(){

        webServer?.dispatcher = MockServerDispatcher().ErrorDispatcher()
        username.perform(ViewActions.typeText("imran"))
        username.perform(ViewActions.clearText())
        username.perform(ViewActions.typeText("imran@wekancode.com"))

        password.perform(ViewActions.typeText("imran"))
        password.perform(ViewActions.clearText())
        password.perform(ViewActions.typeText("Imran@12"))

        closeSoftKeyboard()
        login.check(ViewAssertions.matches(isDisplayed()))
        login.perform(ViewActions.click())
        Assert.assertEquals("User not found",activityLoginRule.activity.loginViewModel.loginResult.value?.error)
    }

    @Test
    fun loginFail_TimeOut(){

        webServer?.dispatcher = MockServerDispatcher().ThrottleDispatcher()
        webServer?.takeRequest(10,TimeUnit.MILLISECONDS)

        username.perform(ViewActions.typeText("imran"))
        username.perform(ViewActions.clearText())
        username.perform(ViewActions.typeText("imran@wekancode.com"))

        password.perform(ViewActions.typeText("imran"))
        password.perform(ViewActions.clearText())
        password.perform(ViewActions.typeText("Imran@12"))

        closeSoftKeyboard()
        login.check(ViewAssertions.matches(isDisplayed()))
        login.perform(ViewActions.click())
        Assert.assertEquals(R.string.unknown_error,activityLoginRule.activity.loginViewModel.loginResult.value?.errorCode)
    }

    @Test
    fun loginSuccess() {

        webServer?.dispatcher = MockServerDispatcher().RequestDispatcher()
        username.perform(ViewActions.clearText())
        username.perform(ViewActions.typeText("imrank@wekan.company"))
        password.perform(ViewActions.clearText())
        password.perform(ViewActions.typeText("wkc@123"))
        closeSoftKeyboard()
        login.check(ViewAssertions.matches(isDisplayed()))
        login.perform(ViewActions.click())

        name = onView(withId(R.id.user_name))
        name.check(ViewAssertions.matches(isDisplayed()))
        name.check(ViewAssertions.matches(withText("imran m")))

    }

    @After
    fun finishTest(){
        IdlingRegistry.getInstance().unregister(activityLoginRule.activity.loginViewModel.idleResource)
        webServer?.shutdown()
    }
}