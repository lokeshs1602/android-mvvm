package com.wk.mvvmboilerplate

import android.app.Instrumentation
import android.content.Context
import android.view.View
import androidx.lifecycle.Observer
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.idling.CountingIdlingResource
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import okhttp3.mockwebserver.MockWebServer
import org.junit.runner.RunWith
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.ui.adapters.DashboardUsersAdapter
import com.wk.mvvmboilerplate.ui.addnewuser.AddNewUserActivity
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivityFragment
import com.wk.mvvmboilerplate.utils.PrefKeys
import com.wk.mvvmboilerplate.utils.PrefUtils
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.content_dashboard.*
import org.junit.*
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
@LargeTest
class Test3DashboardActivityInstrumentTest {

    @get:Rule
    val activityLoginRule = IntentsTestRule(DashboardActivity::class.java,true,false)

    lateinit var addNewUser : ViewInteraction
    lateinit var userList : ViewInteraction

    private var webServer: MockWebServer = MockWebServer()
    lateinit var preferences: PrefUtils

    @Before
    fun setUp(){
        webServer?.start(8080)


        preferences = PrefUtils()
        preferences.appPref = InstrumentationRegistry.getInstrumentation().targetContext.getSharedPreferences(PrefKeys.PREFNAME, Context.MODE_PRIVATE)
        preferences.moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        var user = User(_id = "123456",
            accessToken = "Bearer token",
            email = "imrank@wekan.company",
            firstName = "imran",lastName = "m",profileImageUrl = "",
            refreshToken = "token",
            status = 1
        )
        Log.i("webServer","${webServer?.toProxyAddress()}")
        preferences.setUserPref(user)

        webServer?.dispatcher = MockServerDispatcher().RequestDispatcher()
    }


    @Test
    fun dashboardActionMenuAddNewUser(){
        activityLoginRule.launchActivity(Intent())
        IdlingRegistry.getInstance().register(activityLoginRule.activity.dashBoardViewModel.idleResource)
        userList = onView(withId(R.id.user_list)).check(ViewAssertions.matches(isDisplayed()))

        Espresso.openContextualActionModeOverflowMenu()
        addNewUser = onView(withText("Add New User")).check(ViewAssertions.matches(isDisplayed()))
        addNewUser.perform(ViewActions.click())

        Intents.intended(IntentMatchers.hasComponent(AddNewUserActivity::class.qualifiedName))

    }
    @Test
    fun dashboardUserListSwipeUp(){
        activityLoginRule.launchActivity(Intent())
        IdlingRegistry.getInstance().register(activityLoginRule.activity.dashBoardViewModel.idleResource)
        userList = onView(withId(R.id.user_list)).check(ViewAssertions.matches(isDisplayed()))
        userList.check(ViewAssertions.matches(isDisplayed()))
        userList.perform(ViewActions.swipeUp())


    }

    @Test
    fun dashboardUserListScroll() {
        activityLoginRule.launchActivity(Intent())
        IdlingRegistry.getInstance().register(activityLoginRule.activity.dashBoardViewModel.idleResource)
        userList = onView(withId(R.id.user_list)).check(ViewAssertions.matches(isDisplayed()))

        userList.perform(RecyclerViewActions.scrollToPosition<DashboardUsersAdapter.ViewHolder>(20))


    }

    @After
    fun finishTest(){
        preferences.appPref.edit().clear().commit()

        IdlingRegistry.getInstance().unregister(activityLoginRule.activity.dashBoardViewModel.idleResource)
        webServer?.dispatcher?.shutdown()
        webServer?.shutdown()

    }
}