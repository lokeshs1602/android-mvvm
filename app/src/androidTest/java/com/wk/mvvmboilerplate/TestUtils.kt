package com.wk.mvvmboilerplate

import androidx.test.platform.app.InstrumentationRegistry
import java.io.*


/**
 * Helper function which will load JSON from
 * the path specified
 *
 * @param path : Path of JSON file
 * @return json : JSON from file at given path
 */
object TestUtils{

    @Throws(Exception::class)
    fun getContentFromFile(fileName: String): String {
        var context = InstrumentationRegistry.getInstrumentation().context
        val file = context.assets.open(fileName)
        return String(file.readBytes())
    }

}