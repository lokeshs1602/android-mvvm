package com.wk.mvvmboilerplate

import android.app.Activity
import android.app.Application
import android.util.Log
import com.wk.mvvmboilerplate.di.component.AppComponent
import com.wk.mvvmboilerplate.di.component.DaggerAppComponent
import com.wk.mvvmboilerplate.di.module.AppModule
import com.wk.mvvmboilerplate.TestingRetrofitModule
import com.wk.mvvmboilerplate.di.module.NetModule
import com.wk.mvvmboilerplate.di.module.RetrofitModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


/**
 * Application class to initialize sdk's and other lib/service that were supposed to be initialize on application class
 */

class TestingApp : App(),HasAndroidInjector {

    override fun androidInjector(): AndroidInjector<Any> = activityInjector as AndroidInjector<Any>

    private var applicationComponent: TestingAppComponent? = null

    override fun onCreate() {
        super.onCreate()
        getComponent().inject(this)
    }

    private fun getComponent(): TestingAppComponent{

        if (applicationComponent == null) {
            applicationComponent = DaggerTestingAppComponent.builder().appModule(AppModule(this as Application)).netModule(NetModule()).testingRetrofitModule(
                TestingRetrofitModule()
            )
                .build()
        }
        return applicationComponent!!
    }

}