package com.wk.mvvmboilerplate

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.work.*
import com.facebook.FacebookSdk
import com.wk.mvvmboilerplate.di.component.AppComponent
import com.wk.mvvmboilerplate.di.component.DaggerAppComponent
import com.wk.mvvmboilerplate.di.module.AppModule
import com.wk.mvvmboilerplate.di.module.NetModule
import com.wk.mvvmboilerplate.di.module.RetrofitModule
import com.wk.mvvmboilerplate.utils.Constants
import com.wk.mvvmboilerplate.work.SyncWorker
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject
import io.realm.Realm
import java.util.concurrent.TimeUnit


/**
 * Application class to initialize sdk's and other lib/service that were supposed to be initialize on application class
 */
open class App : Application(), HasAndroidInjector{

    val TAG = App::class.java.name

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        Log.i("application","App")
//        DaggerAppComponent.builder().appModule(AppModule(this)).netModule(NetModule()).build().inject(this)
        FacebookSdk.sdkInitialize(applicationContext)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Realm.init(this)

        // Create charging constraint
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val syncPeriodicWorkRequest =
            PeriodicWorkRequest.Builder(SyncWorker::class.java,15,TimeUnit.MINUTES).setConstraints(constraints).build()

        WorkManager.getInstance(this.baseContext).enqueueUniquePeriodicWork(
            Constants.SYNC_DB_TAG,
            ExistingPeriodicWorkPolicy.REPLACE,syncPeriodicWorkRequest
        )
        getComponent().inject(this)

//        FirebaseInstanceId.getInstance().instanceId
//            .addOnCompleteListener(OnCompleteListener { task ->
//                if (!task.isSuccessful) {
//                    Log.w(TAG, "getInstanceId failed", task.exception)
//                    return@OnCompleteListener
//                }
//
//                // Get new Instance ID token
//                val token = task.result?.token
//
//                // Log and toast
//                val msg =  "TOKEN $token"
//                Log.d(TAG, msg)
//                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
//            })

    }

    private var applicationComponent:AppComponent? = null

    private fun getComponent(): AppComponent {

        Log.i("application","App comp")
        if (applicationComponent == null) {
            applicationComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this)).netModule(NetModule()).retrofitModule(RetrofitModule())
                .build()
        }
        return applicationComponent!!
    }
    override fun androidInjector(): AndroidInjector<Any> = activityInjector as AndroidInjector<Any>

}