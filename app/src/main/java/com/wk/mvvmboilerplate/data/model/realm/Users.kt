package com.wk.mvvmboilerplate.data.model.realm

import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Users : RealmObject() {
    @PrimaryKey
    @field:Json(name = "id")
    var id: String = ""

    @field:Json(name = "email")
    var email: String= ""
    @field:Json(name = "firstName")
    var firstName: String= ""
    @field:Json(name = "lastName")
    var lastName: String= ""

    @field:Json(name = "profileImageUrl")
    var profileImageUrl: String?= ""
    @field:Json(name = "status")
    var status: Int = 0
}