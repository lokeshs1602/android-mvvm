package com.wk.mvvmboilerplate.data.model.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "user")
data class User(

    @field:Json(name = "local_id")
    var local_id: Long = 0,
    @field:Json(name = "_id")
    var _id: String = "",
    @field:Json(name = "accessToken")
    var accessToken: String = "",
    @PrimaryKey
    @field:Json(name = "email")
    var email: String= "",
    @field:Json(name = "firstName")
    var firstName: String= "",
    @field:Json(name = "lastName")
    var lastName: String= "",
    @field:Json(name = "profileImageUrl")
    var profileImageUrl: String?= "",
    @field:Json(name = "refreshToken")
    var refreshToken: String= "",
    @field:Json(name = "status")
    var status: Int = 0
)