package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.dao.UserDao
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.reqres.*
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

/**
 * Repository Class that handles user sign up.
 */

class AddNewUserRepository @Inject constructor(private val userDao: UserDao,private val prefUtil: PrefUtils) {

    /**
     * function to insert user into Room db
     */
    fun insertUser(user: User):Long{
        var userList = userDao.getUserEmail(user.email)
        return if(userList?.size == 0)
             userDao.insertUser(user)
        else -1
    }
}
