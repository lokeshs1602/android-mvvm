package com.wk.mvvmboilerplate.data.remote

import com.wk.mvvmboilerplate.data.remote.reqres.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface{
    /**
     * Login API
     */
    @Headers("Accept: application/json")
    @POST("auth/users/login")
    fun loginApi(@Body request: LoginRequest): Call<LoginReponse>

    /**
     * Signup API
     */
    @Headers("Accept: application/json")
    @POST("users")
    fun signupApi(@Body request: SignupRequest): Call<SignupResponse>

    /**
     * Verify OTP API
     */
    @Headers("Accept: application/json")
    @POST("auth/users/{userId}/verify")
    fun verifyOTPApi(@Path(value = "userId", encoded = true) userId:String,@Body request: VerifyOTPRequest): Call<SignupResponse>

    /**
     * Resend OTP API
     */
    @Headers("Accept: application/json")//auth/users/
    @POST("auth/users/{userId}/verification-code")
    fun resendOTPApi(@Path(value = "userId") userId: String): Call<okhttp3.Response>

    /**
     * User Details API
     */
    @Headers("Accept: application/json")
    @GET("users/{userId}")
    fun userDetailsApi(@Header("Authorization") token:String,@Path(value = "userId", encoded = true) userId:String,
                       @Query(value = "fields") fields: String): Call<UserDetailsReponse>

    /**
     * User Details API
     */
    @Headers("Accept: application/json")
    @GET("users")
    fun allUserDetailsApi(@Header("Authorization") token:String,
                       @Query(value = "fields") fields: String,
                          @Query(value = "sort") sort: String): Call<AllUserDetailsReponse>

    /**
     * UpdateProfile API
     */
    @Headers("Accept: application/json")
    @PATCH("users")
    fun updateProfileApi(@Header("Authorization") token:String,@Body request: UpdateProfileRequest): Call<SignupResponse>

    /**
     * UpdatePassword API
     */
    @Headers("Accept: application/json")
    @PATCH("users")
    fun updatePasswordApi(@Header("Authorization") token:String,@Body request: UpdatePasswordRequest): Call<SignupResponse>

}