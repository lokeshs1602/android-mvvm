package com.wk.mvvmboilerplate.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.data.remote.reqres.*
import com.wk.mvvmboilerplate.utils.PrefUtils
import javax.inject.Inject

/**
 * Repository Class that handles user profile update.
 */

class UpdateProfileRepository @Inject constructor(private val apiInterface: ApiInterface, private val prefUtil: PrefUtils) {

    @Inject
    lateinit var moshi:Moshi

    @Inject
    lateinit var dashboardDataSource:DashboardDataSource
    /**
     * function for updating firstName,lastName
     */
    fun update(firstName: String,lastName: String): Result<User> {
        try {

            val user = prefUtil.getPrefUser()
            val response = apiInterface.updateProfileApi(user?.accessToken!!,UpdateProfileRequest(firstName = firstName,lastName = lastName)).execute()
            Log.i("loggedInUser ", response .toString())
            return when {
                response.body() is SignupResponse -> {
                    val userData = response.body()?.data?.user
                    var prefUser = prefUtil.getPrefUser()
                    prefUser?.firstName = userData?.firstName!!
                    prefUser?.lastName = userData?.lastName!!
                    prefUtil.setUserPref(prefUser!!)
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }
    /**
     * function for updating password
     */
    fun update(password: String): Result<User> {
        try {

            val user = prefUtil.getPrefUser()
            val response = apiInterface.updatePasswordApi(user?.accessToken!!,UpdatePasswordRequest(password = password)).execute()
            Log.i("loggedInUser ", response .toString())
            return when {
                response.body() is SignupResponse -> {
                    val userData = response.body()?.data?.user
                    Result.Success(userData!!)
                }
                response.errorBody()!=null -> {
                    var adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    var error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return Result.Error(resId = R.string.unknown_error)
        }
    }


    fun loadUser():Result<UserData>?{
        return dashboardDataSource.loadUser()
    }
}
