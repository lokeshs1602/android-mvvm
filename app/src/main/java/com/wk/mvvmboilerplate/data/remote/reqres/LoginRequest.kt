package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json

data class LoginRequest(
    @Json(name = "email")
    val email:String?=null,
    @Json(name = "password")
    val password:String?=null
)