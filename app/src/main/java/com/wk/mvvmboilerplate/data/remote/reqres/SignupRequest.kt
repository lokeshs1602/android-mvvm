package com.wk.mvvmboilerplate.data.remote.reqres


import com.squareup.moshi.Json

data class SignupRequest(
    @Json(name = "email")
    val email: String,
    @Json(name = "firstName")
    val firstName: String,
    @Json(name = "lastName")
    val lastName: String,
    @Json(name = "password")
    val password: String?,
    @Json(name = "profileImageUrl")
    val profileImageUrl: String?
)