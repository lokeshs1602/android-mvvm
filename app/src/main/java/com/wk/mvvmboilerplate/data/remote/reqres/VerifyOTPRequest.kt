package com.wk.mvvmboilerplate.data.remote.reqres

import com.squareup.moshi.Json

data class VerifyOTPRequest(
    @Json(name = "verificationCode")
    val verificationCode:String?=null
)