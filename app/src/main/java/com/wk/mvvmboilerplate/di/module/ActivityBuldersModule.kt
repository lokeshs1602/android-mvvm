package com.wk.mvvmboilerplate.di.module

import com.wk.mvvmboilerplate.ui.addnewuser.AddNewUserActivity
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import com.wk.mvvmboilerplate.ui.signup.SignupActivity
import com.wk.mvvmboilerplate.ui.sociallogin.SocialLoginActivity
import com.wk.mvvmboilerplate.ui.splash.SplashActivity
import com.wk.mvvmboilerplate.ui.updateprofile.UpdateProfileActivity
import com.wk.mvvmboilerplate.ui.verifyotp.VerifyOTPActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * class to provide the injection for the activities
 */
@Module
abstract class ActivityBuldersModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeSocialLoginActivity(): SocialLoginActivity

    @ContributesAndroidInjector
    abstract fun contributeSignUpActivity(): SignupActivity

    @ContributesAndroidInjector
    abstract fun contributeDashboardActivity(): DashboardActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeVerifyOTPActivity(): VerifyOTPActivity

    @ContributesAndroidInjector
    abstract fun contributeUpdateProfileActivity(): UpdateProfileActivity

    @ContributesAndroidInjector
    abstract fun contributeAddNewUserActivity(): AddNewUserActivity
}