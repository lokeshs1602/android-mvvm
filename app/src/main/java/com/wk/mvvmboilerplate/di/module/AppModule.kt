package com.wk.mvvmboilerplate.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.wk.mvvmboilerplate.data.model.room.AppDatabase
import com.wk.mvvmboilerplate.data.model.room.dao.UserDao
import com.wk.mvvmboilerplate.utils.Constants
import com.wk.mvvmboilerplate.utils.PrefKeys
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
/**
 * class to provide injection for application level access
 */
@Module
class AppModule (val app:Application){
    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application): AppDatabase = Room.databaseBuilder(app,
        AppDatabase::class.java, Constants.DB_NAME)
        .fallbackToDestructiveMigration()
        .build()

    @Provides
    @Singleton
    fun provideUserDao(
        database: AppDatabase
    ): UserDao= database.userDao()

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application):SharedPreferences = app.getSharedPreferences(PrefKeys.PREFNAME, Context.MODE_PRIVATE)

}