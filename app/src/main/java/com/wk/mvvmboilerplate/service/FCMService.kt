package com.wk.mvvmboilerplate.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.wk.mvvmboilerplate.utils.PrefUtils
import dagger.android.AndroidInjection
import javax.inject.Inject

class FCMService : FirebaseMessagingService() {

    @Inject
    lateinit var prefUtils: PrefUtils

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onNewToken(refreshedToken: String) {
        super.onNewToken(refreshedToken)

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken)

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken)
    }

    private fun sendRegistrationToServer(token: String?) {
        // sending gcm token to server
        Log.e("fire", "sendRegistrationToServer: $token")
//        Instabug.setPushNotificationRegistrationToken(token);
    }

    private fun storeRegIdInPref(token: String) {
        prefUtils.setFCMInstanceId(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e("fire msg",remoteMessage!!.notification.toString())
//        if (Instabug.isInstabugNotification(remoteMessage!!.data)) {
////            // Send the data map.
//            Instabug.showNotification(remoteMessage.data)
//        }
    }
}
