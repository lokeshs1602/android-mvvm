package com.wk.mvvmboilerplate.ui.addnewuser

import android.app.Activity
import android.graphics.Color
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import com.wk.mvvmboilerplate.ui.signup.SignupViewModel
import com.wk.mvvmboilerplate.ui.signup.SignupViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_addnewuser.*

/**
 * Activity for user sign up
 */

class AddNewUserActivity : BaseActivity() {

    @Inject
    lateinit var addNewUserViewModelFactory: AddNewUserViewModelFactory
    private lateinit var addNewUserViewModel: AddNewUserViewModel

    @Inject
    lateinit var signupViewModelFactory: SignupViewModelFactory
    private lateinit var signupViewModel: SignupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_addnewuser)
        AndroidInjection.inject(this)

        addNewUserViewModel = ViewModelProviders.of(this, addNewUserViewModelFactory)
            .get(AddNewUserViewModel::class.java)

        signupViewModel = ViewModelProviders.of(this, signupViewModelFactory)
            .get(SignupViewModel::class.java)

        signupViewModel.signupResult.observe(this@AddNewUserActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.errorCode != null) {
                showToast(loginResult.errorCode)
            }
            if (loginResult.success != null) {
                finish()
            }
        })

        addNewUserViewModel.addNewUserFormState.observe(this@AddNewUserActivity, Observer {
            val addNewUserFormState = it ?: return@Observer

            // disable login button unless both username / password is valid
            add_new_user.isEnabled = addNewUserFormState.isDataValid

            if (addNewUserFormState.emailError != null) {
                email.error = getString(addNewUserFormState.emailError)
            }
        })

        addNewUserViewModel.addNewUserResult.observe(this@AddNewUserActivity, Observer {
            val addNewUserResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (addNewUserResult .error != null) {
                showToast(addNewUserResult .error)
            }
            if (addNewUserResult .errorCode != null) {
                showToast(addNewUserResult .errorCode)
            }
            if (addNewUserResult .success != null) {
                showToast(R.string.add_new_user_success)
                    finish()
            }
        })

        email.afterTextChanged {
            validateForm()
        }
        firstname.afterTextChanged {
            validateForm()
        }
        lastname.afterTextChanged {
            validateForm()
        }
        add_new_user.setOnClickListener {
            if (isConnected){
                signupViewModel.signUp(email.text.toString(),firstname.text.toString(),lastname.text.toString(),"Admin@pass","")
            }else {
                addNewUserViewModel.addNewUser(
                    email.text.toString(),
                    firstname.text.toString(),
                    lastname.text.toString()
                )
            }
        }
    }

    /**
     * validate form on changes to data entered on the form fields
     */
    private fun validateForm(){
        addNewUserViewModel.addNewUserDataChanged(
            email.text.toString(),firstname.text.toString(),lastname.text.toString()
        )
    }

    override fun showConnectionStatus(status:Boolean){
        var msg = if (status) getString(R.string.net_available) else getString(R.string.net_not_available)
        val snack = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val view = snack?.view
        val params = view?.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        view.layoutParams = params
        if(status)
            view.setBackgroundColor(Color.GREEN)
        else
            view.setBackgroundColor(Color.RED)
        snack.show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
