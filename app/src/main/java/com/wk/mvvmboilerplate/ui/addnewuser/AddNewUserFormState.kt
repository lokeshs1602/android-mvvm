package com.wk.mvvmboilerplate.ui.addnewuser

/**
 * Data validation state of the sign up form.
 */
class AddNewUserFormState(
    val emailError: Int? = null,
    val firstNameError: Int? = null,
    val lastNameError: Int? = null,
    val passwordError: Int? = null,
    val cpasswordError: Int? = null,
    val isDataValid: Boolean = false
    )