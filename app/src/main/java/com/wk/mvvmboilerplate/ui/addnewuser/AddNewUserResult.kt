package com.wk.mvvmboilerplate.ui.addnewuser

import com.wk.mvvmboilerplate.data.model.room.entity.User

/**
 * Sign up result : success (user details) or error message.
 */
data class AddNewUserResult(
    val success: User? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
