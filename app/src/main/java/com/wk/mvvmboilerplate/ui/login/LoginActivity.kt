package com.wk.mvvmboilerplate.ui.login

import android.content.Intent
import android.os.AsyncTask
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import com.wk.mvvmboilerplate.ui.dashboard.DashboardActivity
import com.wk.mvvmboilerplate.ui.signup.SignupActivity
import com.wk.mvvmboilerplate.ui.sociallogin.SocialLoginActivity
import com.wk.mvvmboilerplate.ui.verifyotp.VerifyOTPActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

/**
 * Activity for user login
 */

class LoginActivity : BaseActivity() {

    //initializing login view model factory
    @Inject
    lateinit var loginViewModelFactory:LoginViewModelFactory
    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        AndroidInjection.inject(this)

        //initializing login view model
        loginViewModel = ViewModelProviders.of(this, loginViewModelFactory)
            .get(LoginViewModel::class.java)

        //observing the viewmodel for formstate change
        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer
            username.error = null
            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        //observing the viewmodel for login api result change
        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.errorCode != null) {
                showToast(loginResult.errorCode)
            }
            if (loginResult.success != null) {
                when (loginResult.success.status){
                    1 -> navToDash(loginResult.success)
                    0 -> navToVerifyOTP()
                }
                finish()
            }
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE

                loginViewModel.login(username.text.toString(), password.text.toString())
            }
        }

        social_login.setOnClickListener{
            startActivity(Intent(this,SocialLoginActivity::class.java))
        }

        signup.setOnClickListener{
            startActivity(Intent(this, SignupActivity::class.java))
        }
    }

    /**
     * Navigate to dash board on login success
     */
    private fun navToDash(user: User) {
        val welcome = getString(R.string.welcome)
        val displayName = user.firstName
        showToast("$welcome $displayName")
        startActivity(Intent(this,DashboardActivity::class.java))
    }

    /**
     * Navigate to verification of otp if login success and user not verified yet  i.e: status = 0
     */
    private fun navToVerifyOTP() {
        var intent = Intent(this,VerifyOTPActivity::class.java)
        startActivity(intent)
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}