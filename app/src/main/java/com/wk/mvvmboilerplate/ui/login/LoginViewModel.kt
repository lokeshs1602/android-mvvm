package com.wk.mvvmboilerplate.ui.login

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.idling.CountingIdlingResource
import com.wk.mvvmboilerplate.data.remote.LoginRepository
import com.wk.mvvmboilerplate.data.remote.Result

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.realm.Users
import com.wk.mvvmboilerplate.data.model.room.entity.User
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.convertToHashString
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.isEmailValid
import kotlinx.coroutines.*
import okhttp3.internal.wait
import org.junit.Ignore
import javax.inject.Inject

/**
 * View model class for login
 */
class LoginViewModel @Inject constructor(private val loginRepository: LoginRepository) : BaseViewModel() {

    var idleResource = CountingIdlingResource("login")
    /**
     * livedata to observe login form data change
     */
    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm
    /**
     * livedata to observe login api result data
     */
     val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult
    /**
     * function for login api call
     */
    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        idleResource.increment()
        launch {
            val result = loginRepository.login(username, convertToHashString("SHA-512",password))
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> {
                        var loginUser = result.data.user
// authenticate realm sync user for realm cloud db
//                        launch (Dispatchers.IO){
//                            authRealmUser()
//                            withContext(Dispatchers.Main){
                                _loginResult.value =
                                    LoginResult(success = loginUser )
//                            }
//                        }

                    }
                    is Result.Error -> {
                        _loginResult.value = LoginResult(error = result.msg,errorCode = result.resId)
                    }
                    else -> _loginResult.value = LoginResult(errorCode = R.string.unknown_error)
                }
                idleResource.decrement()
            }
        }


    }

    /**
     * function for validating login form data change
     */
    fun loginDataChanged(username: String, password: String) {
        if (!isEmailValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_email)
        } else if (password.isEmpty()) {
            _loginForm.value = LoginFormState(passwordError = R.string.no_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }
}
