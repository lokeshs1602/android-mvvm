package com.wk.mvvmboilerplate.ui.signup

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.ui.base.BaseActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.loading
import kotlinx.android.synthetic.main.activity_signup.password
import kotlinx.android.synthetic.main.activity_signup.signup
import javax.inject.Inject
import com.wk.mvvmboilerplate.ui.login.LoginActivity
import com.wk.mvvmboilerplate.ui.verifyotp.VerifyOTPActivity

/**
 * Activity for user sign up
 */

class SignupActivity : BaseActivity() {

    @Inject
    lateinit var signupViewModelFactory: SignupViewModelFactory
    private lateinit var signupViewModel: SignupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_signup)
        AndroidInjection.inject(this)

        signupViewModel = ViewModelProviders.of(this, signupViewModelFactory)
            .get(SignupViewModel::class.java)

        signupViewModel.signupFormState.observe(this@SignupActivity, Observer {
            val signupFormState = it ?: return@Observer

            // disable login button unless both username / password is valid
            signup.isEnabled = signupFormState.isDataValid

            if (signupFormState.emailError != null) {
                email.error = getString(signupFormState.emailError)
            }
            if (signupFormState.passwordError != null) {
                password.error = getString(signupFormState.passwordError)
            }
            if (signupFormState.cpasswordError != null) {
                confpassword.error = getString(signupFormState.cpasswordError)
            }
            if (signupFormState.cpasswordError != null) {
                confpassword.error = getString(signupFormState.cpasswordError)
            }
        })

        signupViewModel.signupResult.observe(this@SignupActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showToast(loginResult.error)
            }
            if (loginResult.errorCode != null) {
                showToast(loginResult.errorCode)
            }
            if (loginResult.success != null) {
                    navToVerifyOTP()
                    finish()
            }

        })

        email.afterTextChanged {
            validateForm()
        }
        firstname.afterTextChanged {
            validateForm()
        }
        lastname.afterTextChanged {
            validateForm()
        }
        password.afterTextChanged {
            validateForm()
        }
        confpassword.apply {
            afterTextChanged {
                validateForm()
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        signupViewModel.signUp(
                            email.text.toString(),firstname.text.toString(),lastname.text.toString(),
                            password.text.toString(),""
                        )
                }
                false
            }

            signup.setOnClickListener{
                loading.visibility = View.VISIBLE
                signupViewModel.signUp(
                    email.text.toString(),firstname.text.toString(),lastname.text.toString(),
                    password.text.toString(),"")
            }
        }
        back_to_login.setOnClickListener{
             navToLogin()
        }
        // Initialize the AWSMobileClient if not initialized
//        AWSMobileClient.getInstance()
//            .initialize(applicationContext, object : Callback<UserStateDetails> {
//                override fun onResult(userStateDetails: UserStateDetails) {
//                    Log.i("AWSMobileClient",
//                        "AWSMobileClient initialized. User State is " + userStateDetails.userState
//                    )
//                }
//
//                override fun onError(e: Exception) {
//                    Log.e("AWSMobileClient", "Initialization error.", e)
//                }
//            })

    }

    /**
     * validate form on changes to data entered on the form fields
     */
    private fun validateForm(){
        signupViewModel.signUpDataChanged(
            email.text.toString(),firstname.text.toString(),lastname.text.toString(),
            password.text.toString(),confpassword.text.toString()
        )
    }

    /**
     * navigate to login screen
     */
    private fun navToLogin() {
        var intent = Intent(this,LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    /**
     * navigate to verify otp screen
     */
    private fun navToVerifyOTP() {
        var intent = Intent(this, VerifyOTPActivity::class.java)
        startActivity(intent)
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
