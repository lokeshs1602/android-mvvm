package com.wk.mvvmboilerplate.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.SignupRepository
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.data.remote.Result
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.isEmailValid
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import android.util.Log
import com.facebook.FacebookSdk.getApplicationContext
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.convertToHashString
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.validatePassword
import java.io.File

/**
 * View model class for sign up
 */
class SignupViewModel @Inject constructor(private val signupRepository: SignupRepository): BaseViewModel() {
    /**
     * livedata to observe sign up form data change
     */
    private val _signupForm = MutableLiveData<SignupFormState>()
    val signupFormState: LiveData<SignupFormState> = _signupForm
    /**
     * livedata to observe sign up api result data
     */
    private val _signupResult = MutableLiveData<SignupResult>()
    val signupResult: LiveData<SignupResult> = _signupResult
    /**
     * function for sign up api call
     */
    fun signUp(email: String, firstName: String,lastName: String, password: String, profileImageUrl: String){
        // can be launched in a separate asynchronous job
        launch {
            val result = signupRepository.signUp(email, firstName, lastName, convertToHashString("SHA-512",password), profileImageUrl)
            withContext(Dispatchers.Main) {
                if (result is Result.Success) {
                    _signupResult.value =
                        SignupResult(success = result.data)
                } else if (result is Result.Error) {
                    _signupResult.value = SignupResult(error = result.msg,errorCode = result.resId)
                } else {
                    _signupResult.value = SignupResult(errorCode = com.wk.mvvmboilerplate.R.string.unknown_error)
                }
            }
        }
    }
    /**
     * function for validating sign up form data change
     */
    fun signUpDataChanged(username: String,firstName: String,lastName: String, password: String, confpassword: String) {
        if (!isEmailValid(username)) {
            _signupForm.value = SignupFormState(emailError = com.wk.mvvmboilerplate.R.string.invalid_email)
        } else if (firstName.isEmpty()){
            _signupForm.value = SignupFormState(firstNameError = com.wk.mvvmboilerplate.R.string.no_firstname)
        } else if (lastName.isEmpty()){
            _signupForm.value = SignupFormState(lastNameError = com.wk.mvvmboilerplate.R.string.no_lastname)
        } else if (password.isEmpty()) {
            _signupForm.value = SignupFormState(passwordError = com.wk.mvvmboilerplate.R.string.no_password)
        } else if (!validatePassword(password)) {
            _signupForm.value = SignupFormState(passwordError = com.wk.mvvmboilerplate.R.string.invalid_password)
        } else if (confpassword.isEmpty()) {
            _signupForm.value = SignupFormState(cpasswordError = com.wk.mvvmboilerplate.R.string.no_password)
        } else if (password != confpassword){
            _signupForm.value = SignupFormState(cpasswordError = com.wk.mvvmboilerplate.R.string.invalid_conf_password)
        } else {
            _signupForm.value = SignupFormState(isDataValid = true)
        }
    }

}