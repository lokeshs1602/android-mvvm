package com.wk.mvvmboilerplate.ui.splash

/**
 * Data state to decide on navigation to login or dashboard.
 */
data class NavigationDecision(
    val isUserLoggedIn: Boolean = false
)
