package com.wk.mvvmboilerplate.ui.updateprofile

/**
 * Data validation state of the update profile form.
 */
class UpdateProfileFormState(
    val emailError: Int? = null,
    val firstNameError: Int? = null,
    val lastNameError: Int? = null,
    val isDataValid: Boolean = false
    )

/**
 * Data validation state of the change password form.
 */
class ChangePasswordFormState(
    val passwordError: Int? = null,
    val cpasswordError: Int? = null,
    val isDataValid: Boolean = false
)