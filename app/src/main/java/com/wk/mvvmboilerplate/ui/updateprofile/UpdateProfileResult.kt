package com.wk.mvvmboilerplate.ui.updateprofile

import com.wk.mvvmboilerplate.data.model.room.entity.User

/**
 * Update result : success (user details) or error message.
 */
data class UpdateProfileResult(
    val success: User? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
