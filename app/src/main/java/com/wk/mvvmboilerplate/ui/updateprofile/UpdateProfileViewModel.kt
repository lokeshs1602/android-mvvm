package com.wk.mvvmboilerplate.ui.updateprofile

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.remote.UpdateProfileRepository
import com.wk.mvvmboilerplate.ui.login.BaseViewModel
import com.wk.mvvmboilerplate.data.remote.Result
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.isEmailValid
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import android.util.Log
import com.facebook.FacebookSdk.getApplicationContext
import com.wk.mvvmboilerplate.ui.dashboard.DashboardResult
import com.wk.mvvmboilerplate.utils.AppUtils.Companion.convertToHashString
import java.io.File
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


/**
 * View model class for user profile update
 */

class UpdateProfileViewModel @Inject constructor(private val updateProfileRepository: UpdateProfileRepository): BaseViewModel() {
    /**
     * livedata to observe profile form data change
     */
    private val _updateProfileForm = MutableLiveData<UpdateProfileFormState>()
    val updateProfileFormState: LiveData<UpdateProfileFormState> = _updateProfileForm

    /**
     * livedata to observe profile form data change
     */
    private val _updateProgress = MutableLiveData<Int>()
    val updateProgress: LiveData<Int> = _updateProgress

    /**
     * livedata to observe change password form data change
     */
    private val _changePassswordForm = MutableLiveData<ChangePasswordFormState>()
    val changePassswordForm: LiveData<ChangePasswordFormState> = _changePassswordForm

    /**
     * livedata to observe profile update api
     */
    private val _updateProfileResult = MutableLiveData<UpdateProfileResult>()
    val updateProfileResult: LiveData<UpdateProfileResult> = _updateProfileResult

    /**
     * livedata to observe profile data load api
     */
    private val _profileLoadResult = MutableLiveData<UpdateProfileResult>()
    val profileLoadResult: LiveData<UpdateProfileResult> = _profileLoadResult
    /**
     * function to update user first,last name
     */
    fun update( firstName: String,lastName: String){
        // can be launched in a separate asynchronous job
        launch {
            val result = updateProfileRepository.update( firstName, lastName)
            withContext(Dispatchers.Main) {
                if (result is Result.Success) {
                    _updateProfileResult.value =
                        UpdateProfileResult(success = result.data)
                } else if (result is Result.Error) {
                    _updateProfileResult.value = UpdateProfileResult(error = result.msg,errorCode = result.resId)
                } else {
                    _updateProfileResult.value = UpdateProfileResult(errorCode = com.wk.mvvmboilerplate.R.string.unknown_error)
                }
            }
        }
    }
    /**
     * function to change password
     */
    fun changePassword(password: String){
        // can be launched in a separate asynchronous job
        launch {
            val result = updateProfileRepository.update(convertToHashString("SHA-512",password))
            withContext(Dispatchers.Main) {
                if (result is Result.Success) {
                    _updateProfileResult.value =
                        UpdateProfileResult(success = result.data)
                } else if (result is Result.Error) {
                    _updateProfileResult.value = UpdateProfileResult(error = result.msg,errorCode = result.resId)
                } else {
                    _updateProfileResult.value = UpdateProfileResult(errorCode = com.wk.mvvmboilerplate.R.string.unknown_error)
                }
            }
        }
    }
    /**
     * function to validate data changes on change password form
     */
    fun changePassDataChanged( password: String, confpassword: String) {
        if (password.isEmpty()) {
            _changePassswordForm.value = ChangePasswordFormState(passwordError = com.wk.mvvmboilerplate.R.string.no_password)
        } else if (confpassword.isEmpty()) {
            _changePassswordForm.value = ChangePasswordFormState(cpasswordError = com.wk.mvvmboilerplate.R.string.no_password)
        } else if (password != confpassword){
            _changePassswordForm.value = ChangePasswordFormState(cpasswordError = com.wk.mvvmboilerplate.R.string.invalid_conf_password)
        } else {
            _changePassswordForm.value = ChangePasswordFormState(isDataValid = true)
        }
    }
    /**
     * function to validate data changes on user profile form
     */
    fun updateDataChanged(username: String,firstName: String,lastName: String) {
        if (!isEmailValid(username)) {
            _updateProfileForm.value = UpdateProfileFormState(emailError = com.wk.mvvmboilerplate.R.string.invalid_email)
        } else if (firstName.isEmpty()){
            _updateProfileForm.value = UpdateProfileFormState(firstNameError = com.wk.mvvmboilerplate.R.string.no_firstname)
        } else if (lastName.isEmpty()){
            _updateProfileForm.value = UpdateProfileFormState(lastNameError= com.wk.mvvmboilerplate.R.string.no_lastname)
        } else {
            _updateProfileForm.value = UpdateProfileFormState(isDataValid = true)
        }
    }



    /**
     * function to user data in to the user profile form
     */
    fun loadUserData(){
        launch {
            var result = updateProfileRepository.loadUser()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success -> {
                        _profileLoadResult.value = UpdateProfileResult(success = result.data.user)
                    }
                    is Result.Error -> {
                        _profileLoadResult.value = UpdateProfileResult(error = result.msg)
                    }
                    else -> {
                        _profileLoadResult.value = UpdateProfileResult(errorCode = com.wk.mvvmboilerplate.R.string.unknown_error)
                    }
                }
            }
        }

    }
}