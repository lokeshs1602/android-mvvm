package com.wk.mvvmboilerplate.ui.updateprofile

import androidx.lifecycle.ViewModel
import com.wk.mvvmboilerplate.ui.login.BaseViewModelFactory
import javax.inject.Inject

class UpdateProfileViewModelFactory @Inject constructor(private val updateProfileViewModel:UpdateProfileViewModel):BaseViewModelFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UpdateProfileViewModel::class.java)) {
            return updateProfileViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}