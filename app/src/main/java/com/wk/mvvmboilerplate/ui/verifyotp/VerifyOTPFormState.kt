package com.wk.mvvmboilerplate.ui.verifyotp

/**
 * Data validation state of the verify otp form.
 */
data class VerifyOTPFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false
)
