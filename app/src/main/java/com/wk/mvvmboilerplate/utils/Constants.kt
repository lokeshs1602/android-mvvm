package com.wk.mvvmboilerplate.utils

/**
 * Constants for app/intent keys
 */
class Constants{
    companion object{
        val DB_NAME = "boiler.db"
        const val UPDATE_CHANGEPASS = "update_or_changepass"
        const val PERMISSION_REQUSET_CODE = 200
        const val UPLOAD_REQUEST_CODE = 300
        const val SYNC_DB_TAG = "SYNC_DB_TAG"
    }
}

/**
 * Constants for shared preferences key
 */
class PrefKeys {
    companion object{
        val PREFNAME = "app_pref"
        val USERDETAILS = "user_details"
        val FCMID = "fcmregId"
        val REALM_SYNC_USER = "realm_sync_user"
    }
}

/**
 * Constants for network api keys
 */
class ApiConstants{
    companion object{
        const val NOAUTH = "no_auth"
    }
}

