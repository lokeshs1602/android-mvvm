package com.wk.mvvmboilerplate.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Date Constants for formats to display and to convert for calculation
 */
class DateConvertionUtils {
    companion object{
        val DIS_DATEFORMAT = "dd/MM/yyy"
    }
}

/**
 * Extension function for String to date conversion
 */
fun String.toDate():LocalDateTime?{
    return LocalDateTime.parse(this, DateTimeFormatter.ISO_DATE_TIME)
}

/**
 * Extension function for date to string conversion
 */
fun LocalDateTime.formatToPattern(dateStyle:String):String?{
    var dateString:String? = null
    try {
        val formatter = DateTimeFormatter.ofPattern(dateStyle)
        dateString = this.format(formatter)
    }catch (e:Exception){

    }
    return dateString
}