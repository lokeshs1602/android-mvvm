package com.wk.mvvmboilerplate.utils

import android.content.SharedPreferences
import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mvvmboilerplate.data.model.room.entity.User
import javax.inject.Inject

/**
 * class to provide functions to access shared preference values
 */
class PrefUtils @Inject constructor(){

    @Inject
    lateinit var appPref:SharedPreferences

    @Inject
    lateinit var moshi: Moshi

    /**
     * Set user data from api to preference
     */
    fun setUserPref(user:User){
        val adapter = moshi.adapter(User::class.java).lenient()

        var editor = appPref.edit()
        var data = adapter.toJson(user)
        Log.i(PrefKeys.USERDETAILS,data)
        editor.putString(PrefKeys.USERDETAILS,data)
        editor.apply()
    }
    /**
     * Get user data from api to preference
     */
    fun getPrefUser():User?{
        val adapter = moshi.adapter(User::class.java).lenient()
        var data = appPref.getString(PrefKeys.USERDETAILS,null)
        return if(data != null) {
            Log.i(PrefKeys.USERDETAILS,data)
            adapter.fromJson(data)
        }else
            null
    }

    fun setFCMInstanceId(id:String){
        var editor = appPref.edit()
        Log.i(PrefKeys.FCMID,id)
        editor.putString(PrefKeys.FCMID,id)
        editor.apply()
    }

    fun getFCMInstanceId() : String{
        return appPref.getString(PrefKeys.FCMID,"")
    }

    fun setRealmSyncUser(user:String){
        var editor = appPref.edit()
        Log.i(PrefKeys.REALM_SYNC_USER,user)
        editor.putString(PrefKeys.REALM_SYNC_USER,user)
        editor.apply()
    }

    fun getRealmSyncUser() : String{
        return appPref.getString(PrefKeys.REALM_SYNC_USER,"")
    }
}