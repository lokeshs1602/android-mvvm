package com.wk.mvvmboilerplate.work

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.wk.mvvmboilerplate.App
import com.wk.mvvmboilerplate.R
import com.wk.mvvmboilerplate.data.model.room.dao.UserDao
import com.wk.mvvmboilerplate.data.remote.AddNewUserRepository
import com.wk.mvvmboilerplate.data.remote.ApiInterface
import com.wk.mvvmboilerplate.data.remote.reqres.BaseResponse
import com.wk.mvvmboilerplate.data.remote.reqres.SignupRequest
import com.wk.mvvmboilerplate.data.remote.reqres.SignupResponse
import com.wk.mvvmboilerplate.di.component.DaggerAppComponent
import com.wk.mvvmboilerplate.di.module.AppModule
import com.wk.mvvmboilerplate.di.module.NetModule
import com.wk.mvvmboilerplate.utils.AppUtils
import dagger.android.AndroidInjection
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Provider

class SyncWorker @Inject constructor( appContext: Context, workParams:WorkerParameters):CoroutineWorker(appContext,workParams){
    @Inject lateinit var apiInterface: ApiInterface
    @Inject lateinit var userDao: UserDao

    init {
        Log.i("sync init","sync init")

        DaggerAppComponent.builder().appModule(AppModule(appContext.applicationContext as Application)).netModule(NetModule())
            .build().inject(this)
    }

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        return@withContext try {
            // Do something
            syncWork()
            Result.success()
        } catch (error: Throwable) {
            error.printStackTrace()
            Result.failure()
        }
    }


    private fun syncWork(){
        Log.i("sync start","sync start")
            val dbUserList = userDao.getAllUser()
        Log.i("sync size","sync ${dbUserList .size}")
            dbUserList.forEach {

                Log.i("sync loop","sync ${it.local_id} ${it._id}")
                if(it._id.isEmpty()){
                    try {
                        val response = apiInterface.signupApi(SignupRequest(it.email,it.firstName,it.lastName,AppUtils.convertToHashString("SHA-512","Admin@pass"),"")).execute()
                        when {
                            response.body() is SignupResponse -> {
                                Log.i("sync success","${it.email}")
                                userDao.deleteByLocalId(it.local_id)
                            }
                        }
                    }catch (e:Exception){
                        Log.i("sync fail","sync fail ${e.toString()}" )

                    }
                }
        }
    }

}