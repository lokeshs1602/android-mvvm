package com.wk.mvvmboilerplate

import com.wk.mvvmboilerplate.utils.AppUtils
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AppUtilsUnitTest {
    @Test
    fun test_Password_case1() {
        val case1 = AppUtils.validatePassword("imran")
        assertEquals(false, case1)
    }
    @Test
    fun test_Password_case2() {
        val case2 = AppUtils.validatePassword("Admin@123")
        assertEquals(true, case2)
    }

    @Test
    fun test_convertToHashString() {
        val value = AppUtils.convertToHashString("SHA-512","Admin@123")
        assertEquals("6a8eabb9447e2fd817035c282e2275d4fa21f91409dd4726eb071d35e645418192feb3b5f0c60ff836345481bcf3739e3c728e91bd97aa191f92c148be4becae", value)
    }
}
